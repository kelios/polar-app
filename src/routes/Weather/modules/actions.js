import { createActions, } from 'reduxsauce'

// ------------------------------------
// Constants
// ------------------------------------
const { Types, Creators, } = createActions({
  // This generate GET_WEATHER_INFO_SUCCESS action type, and action creator
  //  => Types.GET_WEATHER_INFO
  //  => Creators.getWeatherInfoSuccess(data) => { type: 'GET_WEATHER_INFO_SUCCESS', data: data }
  getWeatherInfo       : [],
  getWeatherInfoSuccess: ['data', ],
  getWeatherInfoFailure: ['error', ],
})

export const WeatherActionTypes = Types
export default Creators
