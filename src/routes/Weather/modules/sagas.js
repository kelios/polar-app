import { LOCATION_CHANGE, } from 'react-router-redux'
import { call, put, take, cancel, takeLatest, } from 'redux-saga/effects'
import api from 'services/api'

import ActionCreators, { WeatherActionTypes, } from './actions'

/**
 * Fetch the weather info as an example.
 * @param {Object} action
 */
export function* fetchWeatherInfo() {
  try {
    const response = yield call(api.weather.getInfo)

    if (response.status === 200 && response.data) {
      yield put(ActionCreators.getWeatherInfoSuccess(response.data.data.list))
    } else {
      // Applicable for response with a non 200 status in the 2xx range
      const errorObject = {
        error       : response,
        errorMessage: 'An unkown error occured',
      }

      yield put(ActionCreators.getWeatherInfoFailure(errorObject))
    }
  } catch (error) {
    yield put(ActionCreators.getWeatherInfoFailure(error))
  }
}

export function* root() {
  const watcher = yield takeLatest(
    WeatherActionTypes.GET_WEATHER_INFO,
    fetchWeatherInfo
  )

  // After location change, we cancel the watcher.
  yield take(LOCATION_CHANGE)
  yield cancel(watcher)
}


export default [
  root,
]
