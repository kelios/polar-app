import { WeatherActionTypes, } from './actions'

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [WeatherActionTypes.GET_WEATHER_INFO]: (state) => ({
    ...state,
    isFetching: true,
    data      : null,
    errors    : null,
  }),

  [WeatherActionTypes.GET_WEATHER_INFO_SUCCESS]: (state, { data, }) => ({
    ...state,
    isFetching: false,
    data,
  }),

  [WeatherActionTypes.GET_WEATHER_INFO_FAILURE]: (state, { error, }) => ({
    ...state,
    isFetching: false,
    errors    : error,
  }),
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isFetching: false,
  data      : null,
  errors    : null,
}

export default function weatherReducer(state = initialState, action) {
  // NOTE: this structure replaces traditional switch structure, returning current state if
  // action.type is note found in this particular ACTION_HANDLERS object
  // You can remplace this export for the createReducer(INITIAL_STATE, HANDLERS) from reduxsauce.
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
