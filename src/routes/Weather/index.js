import { injectReducer, } from 'modules/reducers'
import { injectSagas, } from 'modules/sagas'

export default (store) => ({
  path: '/weather',

  /*  Async getComponent when route matches   */
  getComponent(nextState, cb) {
    /*  Webpack (split point) - async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack -require callback  */
      const WeatherContainer = require('./containers/WeatherContainer').default
      const reducer = require('./modules/reducer').default
      const sagas = require('./modules/sagas').default

      /*  Add the reducer to the store on key 'dashboard'  */
      injectReducer(store, { key: 'weather', reducer, })
      injectSagas(store, sagas)

      /*  Return getComponent   */
      cb(null, WeatherContainer)

    /* Webpack named bundle   */
    }, 'weather')
  },
})
