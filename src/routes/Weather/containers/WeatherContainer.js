import { connect, } from 'react-redux'
import WeatherView from '../components/WeatherView'
import ActionCreators from '../modules/actions'

const mapStateToProps = ({ weather, }) => {
  return {
    isFetching: weather.isFetching,
    data      : weather.data,
    errors    : weather.errors,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getWeatherInfo: () => dispatch(ActionCreators.getWeatherInfo()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherView)
