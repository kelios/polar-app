import React from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import moment from 'moment'
import WeatherDayBox from '../WeatherDayBox'
import './WeatherView.scss'

/**
 * Example View.
 * - Renders the temperature of the last 5 days in Alaska
 */
class WeatherView extends React.Component {
  componentDidMount() {
    this.props.getWeatherInfo()
  }

  /**
   * Helper to parse the data from OpenWeather
   */
  buildBoxes() {
    const weatherDay = _.groupBy(this.props.data, (weather) => {
      return moment(weather.dt_txt).format('YYYY-MM-DD')
    })

    let elBoxes = []

    _.forEach(weatherDay, (info, key) => {
      elBoxes.push(
        <WeatherDayBox
          key={ info[0].dt }
          temp={ info[0].main.temp }
          icon={ info[0].weather[0].icon }
          date={ key }
        />
      )
    })

    return elBoxes
  }

  render() {
    const { isFetching, data, } = this.props

    return (
      <div className="weather-view">
        {
          isFetching && !data ?
            <span className="loading">Loading...</span> :
            <div className="list">
              { this.buildBoxes() }
            </div>
        }
      </div>
    )
  }
}

WeatherView.propTypes = {
  isFetching    : PropTypes.bool.isRequired,
  data          : PropTypes.array,
  getWeatherInfo: PropTypes.func.isRequired,
}

export default WeatherView
