import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import './WeatherDayBox.scss'

/**
 * Box to display the temp of a day.
 * @prop {String} icon: weather icon
 * @prop {String} temp: temp in celcius
 * @prop {String} date: date of the temp.
 */
export const WeatherDayBoxComponent = ({ icon, temp, date, }) => {
  return (
    <div className="weather-day-box">
      <span className="icon">
        <img alt={ icon } src={ `http://openweathermap.org/img/w/${icon}.png` } />
      </span>
      <span className="temp">{ temp }ºC</span>
      <span className="date">{ moment(date).format('MMM Do YY') }</span>
    </div>
  )
}

WeatherDayBoxComponent.propTypes = {
  icon: PropTypes.string.isRequired,
  temp: PropTypes.number.isRequired,
  date: PropTypes.string.isRequired
}

export default WeatherDayBoxComponent
