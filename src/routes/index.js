// We only need to import the modules necessary for initial render
import CoreLayout from 'layouts/CoreLayout'
import Home from './Home'
import Weather from './Weather'

/*  Note: We use react-router PlainRoute object to build route definitions */

export const createRoutes = (store) => ({
  path       : '/',
  component  : CoreLayout,
  indexRoute : Home,
  childRoutes: [
    Weather(store),
  ],
})

export default createRoutes
