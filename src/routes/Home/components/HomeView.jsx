import React from 'react'
import './HomeView.scss'

export const HomeView = () => (
  <div className="home-view">
    <p>
      Welcome. This project has a beautiful readme file,
      dont forget to go through and read it.
    </p>

    <p>
      The weather example uses the OpenWeather api to do a
      demonstration of calls to a server. Dont forget to remove it.
    </p>
  </div>
)

export default HomeView
