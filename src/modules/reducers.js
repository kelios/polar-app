/* eslint no-param-reassign: ["error", { "props": false }] */
import { combineReducers, } from 'redux'
import { reducer as formReducer, } from 'redux-form'
import { routerReducer, } from 'react-router-redux'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    form   : formReducer,
    routing: routerReducer,
    ...asyncReducers,
  })
}

/**
 * Inject a reducer in the store.
 * @param {Object} store  : Global store of the app.
 * @param {String} key    : Key that will have the reducer injected into the store.
 * @param {Object} reducer: Specefic reducer to inject in the store.
 */
export const injectReducer = (store, { key, reducer, }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
