import { all, } from 'redux-saga/effects'

// import { StartupActionTypes } from './startup/actions'
// import * as StartupSagas from './startup/sagas'

const root = function* root() {
  yield all([])
}

/**
 * Inject a saga.
 * @param {Object} store: Global store of the app.
 * @param {Array}  sagas: List of saga functions to inject.
 */
export function injectSagas(store, sagas) {
  // TODO: Add check to validate that sagas is valid (is an array of generators)
  sagas.map(store.runSaga, store)
}

export default root
