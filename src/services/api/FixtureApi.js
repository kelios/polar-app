import { delay, } from 'redux-saga'
import { call, } from 'redux-saga/effects'

// Wrap fixtures in a fake axios response, with an applicable { status: ?, data: passedInFixture }
const fakeAxiosWrap = (status, data) => ({ status, data, })

/**
 * Generate static data for simulate api calls.
 */
export default {
  weather: {
    * getInfo() {
      yield call(delay, 5000)
      const data = require('routes/Weather/fixtures/data.json')
      return fakeAxiosWrap(200, data)
    },
  },
}
