/**
 * Expose all endponts for 'auth'.
 */
const WeatherApi = (api) => {
  return {
    getInfo() {
      api.get('forecast?lat=64.7511&lon=-147.3495&appid=429da572c959a8de4a03a4a1822f5c32&units=metric')
    },
  }
}

export default WeatherApi
