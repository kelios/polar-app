import axios from 'axios'
import config from 'config'
import fixtureApi from './FixtureApi'

// Import the entities apis
import weatherApi from './WeatherApi'

export const api = axios.create({
  baseURL: config.openweathermap_api,
  headers: {
    'Cache-Control': 'no-cache',
  },
  timeout: 10000,
})

let apiObj = {}

if (__DEV__ || __TEST__) {
  apiObj = fixtureApi
}

if (__PROD__) {
  apiObj = {
    weather: weatherApi(api),
  }
}

const apiObject = apiObj

export default apiObject
