import React from 'react'
import { IndexLink, Link, } from 'react-router'
import PropTypes from 'prop-types'
import './CoreLayout.scss'

export const CoreLayout = ({ children, }) => (
  <div className="page-layout layout">
    <header>
      <h1>Polar React</h1>
      <nav>
        <IndexLink to="/" activeClassName="active">Home</IndexLink>
        <Link to="/weather" activeClassName="active">Weather Example</Link>
      </nav>
    </header>
    <main>
      { children }
    </main>
  </div>
)

CoreLayout.propTypes = {
  children: PropTypes.node,
}

CoreLayout.defaultProps = {
  children: null,
}

export default CoreLayout
