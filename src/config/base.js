const base = {
  persistConfig: {
    active        : true,
    reducerVersion: '1',
    blacklist     : [], // reducer keys that you do NOT want stored to persistence here
    whitelist     : [], // Optionally, just specify the keys you DO want stored to
    transforms    : [],
  },
  openweathermap_api: 'http://api.openweathermap.org/data/2.5/',
}

export default base
