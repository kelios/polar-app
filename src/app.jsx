import React from 'react'
import { Router, } from 'react-router'
import { Provider, } from 'react-redux'
import PropTypes from 'prop-types'
import { persistStore, } from 'redux-persist'
import config from 'config'

class App extends React.Component {
  static propTypes = {
    store  : PropTypes.object.isRequired,
    routes : PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  }

  state = {
    rehydrated: false,
  }

  componentWillMount() {
    // Run rehydration of persisted stores with config options required for immutable transforms
    persistStore(this.props.store, config.persistConfig, (error) => {
      if (error) console.error('Rehydrated Error', error)
      // TODO check for rehydration error before continuing with loading the app
      this.setState({ rehydrated: true, })
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.rehydrated !== nextState.rehydrated
  }

  render() {
    if (!this.state.rehydrated) {
      return <span>Loading...</span>
    }

    return (
      <Provider store={ this.props.store }>
        <Router history={ this.props.history } children={ this.props.routes } />
      </Provider>
    )
  }
}

export default App
