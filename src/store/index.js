import makeRootReducer from 'modules/reducers'
import rootSaga from 'modules/sagas'
import createStore from './createStore'

export default () => createStore(makeRootReducer(), rootSaga)
