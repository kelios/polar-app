/* eslint-disable no-underscore-dangle */
import { applyMiddleware, compose, createStore as createReduxStore, } from 'redux'
import { routerMiddleware, } from 'react-router-redux'
import createSagaMiddleware from 'redux-saga'
import { browserHistory, } from 'react-router'
import makeRootReducer from 'modules/reducers'
import rootSaga from 'modules/sagas'

const createStore = (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const middleware = []
  const sagaMiddleware = createSagaMiddleware()
  middleware.push(sagaMiddleware)
  middleware.push(routerMiddleware(browserHistory))

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []
  let composeEnhancers = compose

  if (__DEV__) {
    if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createReduxStore(
    makeRootReducer(),
    initialState,
    composeEnhancers(
      applyMiddleware(...middleware),
      ...enhancers
    )
  )
  store.asyncReducers = {}
  store.runSaga = sagaMiddleware.run
  // kick off root saga
  sagaMiddleware.run(rootSaga)

  if (module.hot) {
    module.hot.accept('../modules/reducers', () => {
      const reducers = require('../modules/reducers').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}

export default createStore
