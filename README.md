# Polar WebApp Boilerplate - React/Redux/Sagas
This project is designed to get you up and running with a full "ready to production" env using moderns techs of front-end. The main features of this project are react/redux/sagas fractal integration, code spliting and async dependecnies injection.

## Table of Contents
1. [Requirements](#requirements)
1. [Installation](#getting-started)
1. [Running the Project](#running-the-project)
1. [Project Structure](#project-structure)
1. [Development](#local-development)
    * [Alias & config](#alias)
    * [Hot Reloading](#hot-reloading)
    * [ESlint](#eslint)
    * [Redux DevTools](#redux-devtools)
1. [Core Arch](#core-arch)
    * [Modules](#modules)
    * [Redux](#redux)
    * [Sagas](#sagas)
    * [Async Modules & Code spliting](#async-modules)
1. [Routing](#routing)
1. [Testing](#testing)
    * [Jest](#jest)
    * [Enzyme](#Enzyme)
1. [Building for Production](#building-for-production)

## Requirements
* node `^5.0.0`
* yarn `^0.23.0` or npm `^3.0.0`

## Installation

After confirming that your environment meets the above [requirements](#requirements), you can create a new project based on `polar-app` by doing the following:

```bash
$ git clone https://roger06@bitbucket.org/kelios/polar-app.git <my-project-name>
$ cd <my-project-name>
```

After that, install the project dependencies. It is recommended that you use [Yarn](https://yarnpkg.com/) for deterministic dependency management, but `npm install` will suffice.

```bash
$ yarn  # Install project dependencies (or `npm install`)
```

## Running the Project

```bash
$ yarn start  # Start the development server (or `npm start`)
```

## Yarn Commands

|`yarn <script>`    |Description|
|-------------------|-----------|
|`start`            |Serves your app at `localhost:3000`|
|`build`            |Builds the application to ./dist|
|`test`             |Runs unit tests with Jest. See [testing](#testing)|
|`test:watch`       |Runs `test` in watch mode to re-run tests when changed|
|`test:coverage`    |Runs `test` and colect coverage|
|`lint`             |[Lints](http://stackoverflow.com/questions/8503559/what-is-linting) the project for potential errors|
|`lint:fix`         |Lints the project and [fixes all correctable errors](http://eslint.org/docs/user-guide/command-line-interface.html#fix)|

## Project Structure

The project structure presented in this project is **fractal**, where functionality is grouped primarily by feature rather than file type. This structure is only meant to serve as a guide, it is by no means prescriptive.

```
.
├── config                   # All build-related code
├── project.config.js        # Main project configuration vars (webpack dev and build)
├── public                   # Static public assets (not imported anywhere in source code)
├── server                   # Express application that provides webpack middleware
│   └── main.js              # Server application entry point
├── src                      # Application source code
│   ├── index.html           # Main HTML page container for app
│   ├── main.js              # Application bootstrap and rendering
│   ├── normalize.js         # Browser normalization and polyfills
│   ├── config               # Project internal configuration vars (endpoints, keys, etc)
│   ├── components           # Global Reusable Components
│   ├── modules              # Global reducers/sagas/actions for the application
│   │   ├── reducers.js      # Root Reducer registry & injection
│   │   ├── sagas.js         # Root Sagas registry & injection
│   │   └── example          # Example global module
│   │       ├── actions      # Actions for example module
│   │       ├── reducer      # Reducer for example module
│   │       └── sagas        # Sagas for example module
│   ├── layouts              # Components that dictate major page structure
│   │   └── CoreLayout       # Core application layout in which to render routes
│   ├── routes               # Main route definitions and async split points
│   │   ├── index.js         # Bootstrap main application routes with store
│   │   ├── Home             # Fractal route
│   │   │   ├── index.js     # Route definitions and async split points
│   │   │   ├── assets       # Assets required to render components
│   │   │   ├── components   # Presentational React Components
│   │   │   └── routes **    # Fractal sub-routes (** optional)
│   │   └── Weather          # Fractal route (example)
│   │       ├── index.js     # Weather route definition (example)
│   │       ├── container    # Connect components to actions and store (example)
│   │       ├── fixtures     # Dummy data to use in the requests (example)
│   │       ├── modules      # Collections of reducers/constants/actions (example)
│   │       └── routes **    # Fractal sub-routes (** optional) 
│   ├── services             # Redux-specific pieces
│   │   └── api              # Api and Fixture services
│   │      ├── index.js      # Api creation and configuration
│   │      ├── FixtureApi.js # Api simualte calls (grouped by entities)
│   │      └── WeatherApi.js # All call related to the weather entity (Example)
│   ├── store                # Redux-specific pieces
│   │   ├── createStore.js   # Create and instrument redux store
│   │   └── index.js         # Expose the creation of the store.
│   └── styles               # Application-wide styles (generally settings)
│       └── globals          # The content of this folder will be injected in all sass files.
│           ├── vars.scss    # Global Sass vars.
│           └── mixins.scss  # Global sass mixins
└── __tests__                # Unit tests
```

## Development

### Alias & Config
For the imports of modules in the project you can import all from relative path to `/src/` folder.
For example if you wanna import a component inside `src/componetns/MyComponen`, you don't need to specify any relative path, the import will be like this: `import MyComponent from components/MyComponent`
We recomend use only relative paths if the diference of level between folder its 1 or 2.

Also you can access to the correct configuration file for your current env doing: `import config from config`
Webpack will be in charge of exposing the correct configuration file based on the environment.

### Hot Reloading

Hot reloading is enabled by default when the application is running in development mode (`yarn start`). This feature is implemented with webpack's [Hot Module Replacement](https://webpack.github.io/docs/hot-module-replacement.html) capabilities, where code updates can be injected to the application while it's running, no full reload required. Here's how it works:

* For **JavaScript** modules, a code change will trigger the application to re-render from the top of the tree. **Global state is preserved (i.e. redux), but any local component state is reset**. This differs from React Hot Loader, but we've found that performing a full re-render helps avoid subtle bugs caused by RHL patching.

* For **Sass**, any change will update the styles in realtime, no additional configuration or reload needed.

### ESlint
The project is configured to run a eslint each time it updates. Errors and warnings will be displayed on the console. This can be modified in the webpack configuration file.
The eslint configuration can be founded in the .eslintrc. This extends the base configuration of [eslint-config-airbnb](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb) and add some custom rules.

### Redux DevTools

**We recommend using the [Redux DevTools Chrome Extension](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd).**
Using the chrome extension allows your monitors to run on a separate thread and affords better performance and functionality. It comes with several of the most popular monitors, is easy to configure, filters actions, and doesn't require installing any packages in your project.

## Core Arch
TODO: write this
### Modules
TODO: write this
### Redux
TODO: write this
### Sagas
TODO: write this

### Async Modules & Code spliting
TODO: write this
## Routing
We use `react-router v3` [route definitions](https://github.com/ReactTraining/react-router/blob/v3/docs/API.md#plainroute) (`<route>/index.js`) to define units of logic within our application. See the [project structure](#project-structure) and [core arch](#core-arch) sections for more information. 
(NOTE: This project use the v3.0 of react-router beacuse the v4 dosn`t support PlainRoute configuration any more.)

## Testing
To add a unit test, create a `.spec.js` file anywhere inside of the porject. Jest will automatically find these files. Here are a few important plugins and packages available to you during testing:

### Jest
TODO: Write this
### Enzyme
TODO: Write this

## Building for Production

### Deployment

Out of the box, the boilerplate will build in the `./dist` folder when you run `yarn build`.
You can deploy your app by serving the content of the this folder. Make sure to direct incoming route requests to the root `./dist/index.html` file so that the client application will be loaded; react-router will take care of the rest. 

